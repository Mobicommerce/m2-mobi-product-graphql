<?php
 
namespace Mobicommerce\MobiProductGraphQl\Model\Resolver;
 
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
 
class MobiAttributes implements ResolverInterface
{
    private $productsgraphqlDataProvider;
 
    /**
     * @param DataProvider\Productsgraphql $productsgraphqlDataProvider
     */
    public function __construct(
        \Mobicommerce\MobiProductGraphQl\Model\Resolver\DataProvider $productsgraphqlDataProvider
    ) {
        $this->productsgraphqlDataProvider = $productsgraphqlDataProvider;
    }
 
    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        //by SKU
        //$sku = $this->getSku( $args);
        /** @var ProductInterface $product */
        $product = $value['model'];
        $sku = $product->getSku();
       // $productsData = $this->productsgraphqlDataProvider->getAttributesBySku( $sku );

         $productsData = $this->productsgraphqlDataProvider->getAttributesBySkuN($product);
        //echo "<pre>sss"; print_r($productsData); exit;
        return $productsData;
    }
}
